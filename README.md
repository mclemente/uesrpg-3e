# UESRPG d100
Please consider supporting the development of this system on Patreon here: https://www.patreon.com/swordsandstones

A system and a few compendiums used to play the UESRPG game. The system is basd off of the Simple Worldbuilding system developed by Atropos. Special thanks to 2Minute Tabletop and to drhodesw for the tokens and help creating the compendiums.

Express permission to use the artwork and tokens included in the compendiums of this system was given by 2MinuteTabletop and the copyright holder.

You can find the UESRPG rules here: https://docs.google.com/document/d/1pTgTN2aJUoY95JtquowagfUJLL7tCQYhzJKcCAcbvio/edit

You can find the lively UESRPG Discord Community here: https://discord.gg/KAkXdf9

<b>Actor Sheet Types:</b> 
- Character Sheet
- NPC Sheet

**Item Sheet Types:**
- Item
- Armor
- Ammunition
- Weapon
- Combat Style
- Spell
- Trait
- Talent
- Power

<b>Compendiums:</b>
- Creatures: Full stats and tokens on creatures from Scrolls of People, Beasts, Undeath, Oblivion, Ayleids, Dwemer, etc. More will be added in the future
- Items: Full stats and hand drawn images for all weapons/armor, generic alchemy ingredients, and soul gems in core rulebook along with some other commonly used items
- Spells: Full stats for all spells included in the core rulebook
- Macros: Macro library for rolling most dice rolls on the character sheet for those who prefer to use the hotbar instead of rolling directly from the sheet
- Talents: A compendium filled with all of the core rulebook talents in addition to many others from the Scroll of Oblivion, Book of Circles, and others
- Traits: A compendium filled with many of the traits in the core rulebook among others
- Powers: A compendium filled with many of the powers from the core rulebook and others
- NPC Build Reference: An All-in-One type of compendium useful for quickly creating NPC's by dragging powers/traits/talents directly onto NPC sheets
- Loot Tables: A small collection of useful rollable tables for GM use

<b>Features:</b>
- Quickly equip armor, track ammo qty, and roll weapon damage directly from the character sheet
- Dropdown characteristic value for skill base so GM's can have the freedom to ask for unique or uncommon skill/characterstic combinations.
- Encumbrance levels automatically impose penalties based on their current carry weight.
- Ticking the "Wounded" checkmark on the Combat tab automatically imposes a -20 penalty to all skill rolls and a -2 penalty to initiative rolls.
- Auto-sorting inventory tab for Item, Armor, and Weapon types.
- Lucky/Unlucky Number determination in skill/characteristic/combat rolls if data is input into the Lucky/Unlucky Number fields on the Core Tab.
- Certain talents/traits automatically add resistances/weaknesses and affect your character's stats directly upon adding to your sheet. Most have been designed to be toggled on/off as well. 
