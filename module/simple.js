// Import Modules
import { UESRPG } from "./config.js";
import { SimpleActor } from "./actor.js";
import { npcSheet } from "./npc-sheet.js";
import { SimpleActorSheet } from "./actor-sheet.js";
import { SimpleItem } from "./item.js";
import { SimpleItemSheet } from "./item-sheet.js";

// Import Helpers
import * as migrations from "./migration.js";

/* -------------------------------------------- */
/*	Foundry VTT Initialization									*/
/* -------------------------------------------- */

Hooks.once("init", async function() {
	console.log(`Initializing UESRPG System`);

	/**
	 * Set an initiative formula for the system
	 * @type {String}
	 */
	CONFIG.Combat.initiative = {
		formula: "1d6 + @initiative.base",
		decimals: 0
	};

	// Record Configuration Values
	CONFIG.UESRPG = UESRPG;

	// Define custom Entity classes
	CONFIG.Actor.documentClass = SimpleActor;
	CONFIG.Item.documentClass = SimpleItem;

	// Register sheet application classes
	Actors.unregisterSheet("core", ActorSheet);
	Actors.registerSheet("uesrpg-d100", SimpleActorSheet, {
		types: ["character"],
		makeDefault: true,
		label: "Default UESRPG Character Sheet"
	});
	Actors.registerSheet("uesrpg-d100", npcSheet, {
		types: ["npc"],
		makeDefault: true,
		label: "Default UESRPG NPC Sheet"
	});
	Items.unregisterSheet("core", ItemSheet);
	Items.registerSheet("uesrpg-d100", SimpleItemSheet, {
		makeDefault: true,
		label: "Default UESRPG Item Sheet"
	});

	// Register system settings
	/**
	 * Track the system version upon which point a migration was last applied
	 */
	game.settings.register("uesrpg-d100", "systemMigrationVersion", {
		name: "System Migration Version",
		scope: "world",
		config: false,
		type: String,
		default: 1.32
	});
	
	game.settings.register("uesrpg-d100", "legacyUntrainedPenalty", {
		name: "Legacy Untrained Penalty",
		hint: "Checking this option enables the UESRPG v2 penalty for Untrained skills at -20 instead of the standard -10. Must refresh the client manually (F5) after selecting this option to see the changes on actor sheets.",
		scope: "world",
		config: true,
		default: false,
		type: Boolean
	});
});

Hooks.once("ready", function() {
	// Determine whether a system migration is required and feasible
	if ( !game.user.isGM ) return;
	const currentVersion = game.settings.get("uesrpg-d100", "systemMigrationVersion");
	const NEEDS_MIGRATION_VERSION = 1.32;
	const COMPATIBLE_MIGRATION_VERSION = 1.32;
	const needsMigration = isNewerVersion(NEEDS_MIGRATION_VERSION, currentVersion);
	if ( !needsMigration ) return game.settings.set("uesrpg-d100", "systemMigrationVersion", game.system.data.version);

	// Perform the migration
	if ( isNewerVersion(COMPATIBLE_MIGRATION_VERSION, currentVersion) ) {
		const warning = `Your UESRPG system data is from too old a Foundry version and cannot be reliably migrated to the latest version. The process will be attempted, but errors may occur.`;
		ui.notifications.error(warning, {permanent: true});
	}
	migrations.migrateWorld();
});